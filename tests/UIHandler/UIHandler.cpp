#include <gtest/gtest.h>

#include <UIHandler/UIHandler.h>

TEST(UIHandler, initRts) {
    char* argv = "init";
    auto initRes = initHRTS(1, &argv);
    ASSERT_EQ(initRes, E_FN_SUCCESS);
    auto clsRes = closeHRTS();
    ASSERT_EQ(clsRes, E_FN_SUCCESS);
}
