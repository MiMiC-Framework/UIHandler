#include <gtest/gtest.h>
#include <SysUtils/ProcInspection.h>
#include <iostream>

#if (defined _WIN32)
#include <Processthreadsapi.h>
#endif

TEST(ProcInspection, GetRunningProcs)
{
    ProcId* ids = (ProcId*)malloc(sizeof(ProcId)*ST_RUNNING_PROCS);
    ProcId_A pIdsArr { ids, ST_RUNNING_PROCS, 0 };
    auto res = getRunningProcs(&pIdsArr);

    free(ids);

    ASSERT_EQ(res, E_FN_SUCCESS);
}

// Returns the last Win32 error, in a self-contained type.
std::string GetLastErrorAsString()
{
    DWORD errorMessageID = ::GetLastError();
    if(errorMessageID == 0)
        return std::string();

    LPSTR messageBuffer = nullptr;
    size_t size =
        FormatMessageA(
            FORMAT_MESSAGE_ALLOCATE_BUFFER  |
            FORMAT_MESSAGE_FROM_SYSTEM      |
            FORMAT_MESSAGE_IGNORE_INSERTS,
            NULL,
            errorMessageID,
            MAKELANGID(
                LANG_NEUTRAL,
                SUBLANG_DEFAULT
            ),
            (LPSTR)&messageBuffer,
            0,
            NULL);

    std::string message(messageBuffer, size);

    //Free the buffer.
    LocalFree(messageBuffer);

    return message;
}

TEST(ProcInspection, GetProcName )
{
    char* pNameStr = (char*)malloc(sizeof(char)*ST_PROC_NAME_SZ);
    Char_A pName { pNameStr, ST_PROC_NAME_SZ, 0 };

    auto res = getProcName(GetCurrentProcessId(), &pName);

    if (res == E_GV_SYS_WIN32ERROR) {
        std::cout << GetLastErrorAsString() << std::endl;
    }

    std::string name(pName.content, pName.content + pName.offset);
    free(pName.content);

    ASSERT_EQ(name, "Tests.exe");
}

TEST(ProcInspection, FindProcByName)
{
    ProcId* ids = (ProcId*)malloc(sizeof(ProcId)*ST_RUNNING_PROCS);

    char* pName = "Tests.exe";
    Char_A pNameArr { pName, sizeof(char)*11, sizeof(char)*11 };

    ProcId_A pIdsArr { ids, ST_RUNNING_PROCS, 0 };
    auto res = findProcByName(&pNameArr, &pIdsArr);

    ASSERT_EQ(res, E_FN_SUCCESS);

    auto matchingPid = *pIdsArr.content;
    free(pIdsArr.content);

    ASSERT_EQ(matchingPid, GetCurrentProcessId());
}

TEST(ProcInspection, GetProcBinPath)
{
    TCHAR pathData[MAX_PATH];
    TChar_A path { pathData, MAX_PATH, 0 };

    auto pId = GetCurrentProcessId();
    auto err = getProcBinPath(pId, &path);

    ASSERT_EQ(err, E_FN_SUCCESS);

    std::string pathS(pathData);
    ASSERT_TRUE(pathS.find("Tests.exe", 0) > 0);
}
