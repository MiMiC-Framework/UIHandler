#include <UIHandler/UIHandler.h>
#include <UIHandler/Error.h>

#include <condition_variable>
#include <iostream>
#include <functional>
#include <mutex>
#include <thread>
#include <stdint.h>

#include <Windows.h>
#include <gtest/gtest.h>
#include <objbase.h>
#include <OleAuto.h>
#include <UIAutomationClient.h>

struct AppHandlers {
    HANDLE pHandle;
    HANDLE tHandle;
};

TEST(FindAppWindow, FindWindowWithApplicationData) {
    std::thread([]() {
        CoInitializeEx(NULL, COINIT_MULTITHREADED);
        ASSERT_EQ(initHRTS(0, NULL), E_FN_SUCCESS);

        auto crrProcessId = GetCurrentProcessId();

        TCHAR* path = "C:\\Windows\\explorer.exe";

        AppInfo appInfo {0, "Test", path };
        IUIAutomationElement* elem = NULL;

        auto res = findWindow(appInfo, &elem);

        ASSERT_TRUE(elem != NULL);
        ASSERT_EQ(res, E_FN_SUCCESS);
        ASSERT_EQ(closeHRTS(), E_FN_SUCCESS);
        CoUninitialize();
    }).join();
}

VOID launchApp(LPCTSTR appName, AppHandlers* handlers)
{
    // additional information
    STARTUPINFO si;
    PROCESS_INFORMATION pi;

    // set the size of the structures
    ZeroMemory(&si, sizeof(si));
    si.cb = sizeof(si);
    ZeroMemory(&pi, sizeof(pi));

    // start the program up
    CreateProcess(
        appName,    // the path
        "",         // Command line
        NULL,       // Process handle not inheritable
        NULL,       // Thread handle not inheritable
        FALSE,      // Set handle inheritance to FALSE
        0,          // No creation flags
        NULL,       // Use parent's environment block
        NULL,       // Use parent's starting directory
        &si,        // Pointer to STARTUPINFO structure
        &pi         // Pointer to PROCESS_INFORMATION structure (removed extra parentheses)
    );

     // Wait until child process exits.
    WaitForSingleObject( pi.hProcess, INFINITE );// Close process and thread handles.

    CloseHandle(pi.hProcess);
    CloseHandle(pi.hThread);
}

// This is the main message handler for the program.
LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
    // sort through and find what code to run for the message given
    switch(message) {
        // this message is read when the window is closed
        case WM_DESTROY:
        {
            // close the application entirely
            PostQuitMessage(0);
            return 0;
        } break;
    }

    // Handle any messages the switch statement didn't
    return DefWindowProc (hWnd, message, wParam, lParam);
}

/**
 * @brief Function to create a dummy window unable to process menssages.
 * @details
 * We want a dummy function unable to show and process any message, for that
 * the best is create one window without the attribute: WNDCLASSEX::lpfnWndProc.
 * With that trick code will exit naturally without process any message.
 */
HWND createDummyWindow() {
    const char CLASS_NAME[] { "DUMMY" };

    WNDCLASS wc = { };

    wc.lpfnWndProc   = WindowProc;
    wc.hInstance     = GetModuleHandle(nullptr);
    wc.lpszClassName = CLASS_NAME;

    RegisterClass(&wc);

    HWND wHandle = CreateWindowEx(
        WS_EX_CLIENTEDGE,
        CLASS_NAME,
        "DummyWindow",
        WS_OVERLAPPEDWINDOW,
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        240,
        120,
        NULL,
        NULL,
        GetModuleHandle(nullptr),
        NULL
    );

    ShowWindow(wHandle, SW_SHOW);
    UpdateWindow(wHandle);

    return wHandle;
}

TEST(DetectWindowOpenning, DetectWindowOpeningEvent) {
    std::thread([]() {
        TCHAR pathData[MAX_PATH];
        TChar_A path { pathData, MAX_PATH, 0 };
        auto cPId = GetCurrentProcessId();
        auto err = getProcBinPath(cPId, &path);

        ASSERT_EQ(err, E_FN_SUCCESS);

        std::string pathS(pathData);

        AppInfo appInfo { 0, "Tests", pathS };

        std::mutex m;
        std::condition_variable cv;
        std::unique_lock<std::mutex> lock(m);

        // Initialize MTA thread, requierement for COM call to UIA
        CoInitializeEx(NULL, COINIT_MULTITHREADED);

        ASSERT_EQ(initHRTS(0, NULL), E_FN_SUCCESS);

        SVar<UIEventQueue> sEventQueue { UIEventQueue{} };
        SVar<LogQueue> sLogQueue { LogQueue{} };

        EventHndResrcs eHResources;

        auto windowDetectError =
            detectWindowOpening(
                appInfo,
                0,
                sEventQueue,
                sLogQueue,
                eHResources
            );

        ASSERT_EQ(windowDetectError, S_OK);

        HWND wHandle;
        int pId;

        std::thread dWindowThread([&cv,&wHandle, &pId]() {
            wHandle = createDummyWindow();
            pId = GetCurrentProcessId();

            MSG Msg;
            // Handle the message loop for the window.
            cv.notify_all();
            while(GetMessage(&Msg, NULL, 0, 0) > 0) {
                TranslateMessage(&Msg);
                DispatchMessage(&Msg);
            }
        });

        cv.wait(lock);
        lock.unlock();

        UIEventQueue uiEventQueue = takeSVar(sEventQueue);
        UIEvent uiEvent = uiEventQueue.front();
        UIA_HWND nativeHandle = uiEvent.eventPl.pVoid;

        SendMessage(wHandle, WM_CLOSE, 0, 0);

        dWindowThread.join();

        eHResources.handler->Release();
        eHResources.rootElem->Release();

        removeAllEventHandlers();

        // Uninitialize MTA thread, requierement for COM call to UIA
        CoUninitialize();

        ASSERT_EQ(nativeHandle, wHandle);
    }).join();
}

// // Simple Example
// TEST(SafeArrayUsageExample, SafeArray) {
//     SAFEARRAY* arr;
//     SAFEARRAYBOUND rgsabound[2];
//     rgsabound[0].lLbound = 0;
//     rgsabound[0].cElements = 5;
//     rgsabound[1].lLbound = 0;
//     rgsabound[1].cElements = 5;
//     arr = SafeArrayCreate(VT_UINT, 2, rgsabound);
//
//     unsigned int* pData;
//     auto hDataRes = SafeArrayAccessData(arr, (void**)&pData);
//
//     pData[0] = 1;
//     pData[4] = 5;
//     *(pData + 5) = 2;
//
//     // Bound cheking.
//     LONG bound;
//     SafeArrayGetLBound(arr, 2, &bound);
//
//     // std::cout << "Bound: " << bound << "\r\n";
//     // std::cout << "Data: " << pData[bound] << "\r\n";
//
//     SafeArrayUnaccessData(arr);
// }

