#include <SysUtils/ProcInspection.h>
#include <FUtils/Ret/Either.h>

#include <algorithm>
#include <iterator>
#include <string>
#include <utility>
#include <vector>

#ifdef _WIN32
#include <Psapi.h>
#endif

using std::vector;
using std::string;
using std::pair;

enum class ProcNameError {
#ifdef _WIN32
    FailedOpenProc = 0,
    FailedEnumProcModules = 1,
    FailedGetModuleBaseName = 2
#endif
};

error_t getRunningProcs(struct ProcId_A* pIdArr) {
#ifdef _WIN32
    ProcId* ids = pIdArr->content;
    size_t arrSize = pIdArr->size;

    if (ids == NULL) {
        return E_FN_INVAL_INIT;
    }

    DWORD pBytesRet = 0;
    auto res = EnumProcesses(pIdArr->content,
                             ((DWORD)pIdArr->size)*sizeof(DWORD),
                             &pBytesRet);
    pIdArr->offset = pBytesRet/sizeof(ProcId);

    if (res == 0) {
        return E_GV_SYS_WIN32ERROR;
    }

    if ((pBytesRet)/sizeof(ProcId) == arrSize) {
        return E_FN_INVAL_ARRSZ;
    }

    return E_FN_SUCCESS;
#endif
}

error_t getProcName(ProcId procId, struct Char_A* pName) {
#ifdef _WIN32
    // Check if parameter is initialized.
    if (pName->content == NULL) {
        return E_FN_INVAL_INIT;
    }

    HANDLE hnd = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, procId);
    if (hnd == NULL) {
        return E_GV_SYS_WIN32ERROR;
    }

    // We just want the first module, the one that refers to the executable
    // itself, it there where the info about the name lives.
    HMODULE mainModule = NULL;
    DWORD sizeForModules = 0;

    auto epmRes = EnumProcessModules(hnd, &mainModule, sizeof(HMODULE), &sizeForModules);
    if (epmRes == 0) {
        return E_GV_SYS_WIN32ERROR;
    }

    LPTSTR rawProcName = (LPTSTR)malloc(sizeof(CHAR)*ST_PATH_SIZE);

    uint32_t nameSize =
        GetModuleBaseName(hnd, mainModule, rawProcName, ST_PATH_SIZE);

    // Check if the parameter name size is enought for the actual name size
    if (nameSize > pName->size) {
        free(rawProcName);
        return E_FN_INVAL_ARRSZ;
    }

    #if UNICODE
    // We need to convert the LPTSTR to LPSTR so we can use then
    // in normal std::strings, for that we are going to use
    // 'WideCharToMultipleByte' function. Always using UTF-8.
    int32_t wNameSize =
        WideCharToMultiByte(
            CP_UTF8
            , 0
            , rawProcName
            , (int32_t)ST_PATH_SIZE
            , NULL
            , 0
            , NULL
            , NULL);

    if (wNameSize == 0) {
        free(rawProcName);
        return E_SYSERROR;
    }

    wNameSize = WideCharToMultiByte(
                CP_UTF8
                , 0
                , rawProcName
                , (int32_t)ST_PATH_SIZE
                , pName->content
                , size
                , NULL
                , NULL);
    pName->offset = wNameSize;

    if (wNameSize == 0) {
        free(rawProcName);
        return E_SYSERROR;
    }

    free(rawProcName);
    return EF_SUCCESS;

    #else
    memcpy(pName->content, rawProcName, nameSize);
    pName->offset = nameSize;

    free(rawProcName);
    return E_FN_SUCCESS;
    #endif
#elif __linux
#endif
}

vector<string> filterNameErr(vector<Either<string, ProcNameError>> resL) {
    std::vector<std::string> names {};

    for (auto&& res : resL) {
        PMatch(res,
            [&names](auto str) {
                names.push_back(str);
            },
            [](auto procNameError) {}
        );
    }

    return names;
}

Either<error_t, vector<ProcId>> getRunningProcs() {
    ProcId* pIds = (ProcId*)malloc(sizeof(ProcId)*ST_RUNNING_PROCS);
    ProcId_A pIdsArr { pIds, ST_RUNNING_PROCS, 0 };

    auto err = getRunningProcs(&pIdsArr);

    if (err == E_GV_SYS_WIN32ERROR) {
        free(pIdsArr.content);
        return MLeft(err);
    } else if (err == E_FN_INVAL_ARRSZ) {
        error_t newRes = E_FN_INVAL_ARRSZ;

        while (newRes == E_FN_INVAL_ARRSZ) {
            free(pIdsArr.content);
            auto newArrSize = ST_RUNNING_PROCS*2;

            pIdsArr.content = (ProcId*)malloc(sizeof(ProcId)*newArrSize);
            pIdsArr.size = newArrSize;
            newRes = getRunningProcs(&pIdsArr);
        }

        if (newRes == E_GV_SYS_WIN32ERROR) {
            free(pIdsArr.content);
            return MLeft(newRes);
        }
    }

    vector<ProcId> res(pIdsArr.content, pIdsArr.content + pIdsArr.offset);
    free(pIdsArr.content);

    return MRight(res);
}

error_t findProcByName(struct Char_A* pName, struct ProcId_A* pIds) {
#ifdef _WIN32
    if (pName == NULL || pIds == NULL) {
        return E_FN_INVAL;
    }

    bool checkInVALS =
        pName->content == NULL      ||
        pName->size == 0            ||
        pIds->content == NULL        ||
        pIds->size == 0;

    if (checkInVALS) {
        return E_FN_INVAL;
    }

    auto curPIds = getRunningProcs();

    error_t res =
        PMatch(curPIds,
            [](auto err) -> error_t {
                return err;
            },
            [&pName, &pIds](auto allPIds) -> error_t {
                string _pName(pName->content, (pName->content + pName->size));
                vector<pair<string, ProcId>> namePIdsPairs;

                std::transform(
                    allPIds.begin(),
                    allPIds.end(),
                    std::back_inserter(namePIdsPairs),
                    [](auto pId) {
                        char* name = (char*)malloc(sizeof(char)*ST_PROC_NAME_SZ);
                        Char_A pName { name, ST_RUNNING_PROCS, 0 };
                        getProcName(pId, &pName);

                        string sName(pName.content, pName.offset);
                        free(name);

                        pair<string, ProcId> res(sName, pId);

                        return res;
                    }
                );

                vector<pair<string,ProcId>> matchingPairs;

                std::copy_if(
                    namePIdsPairs.begin(),
                    namePIdsPairs.end(),
                    std::back_inserter(matchingPairs),
                    [&] (auto p) {
                        if (p.first == "") {
                            return false;
                        } else {
                            return  _pName.find(p.first) != std::string::npos;
                        }
                    }
                );

                vector<ProcId> matchingPIds;

                std::transform(
                    matchingPairs.begin(),
                    matchingPairs.end(),
                    std::back_inserter(matchingPIds),
                    [] (auto p) {
                        return p.second;
                    }
                );

                if (matchingPIds.size() > pIds->size) {
                    return E_FN_INVAL_ARRSZ;
                }

                memcpy(
                    pIds->content,
                    matchingPIds.data(),
                    sizeof(ProcId)*matchingPIds.size()
                );

                pIds->offset = matchingPIds.size();

                return E_FN_SUCCESS;
            }
        );

    return res;
#endif
}

error_t getProcBinPath(ProcId pId, struct TChar_A* filePath) {
#ifdef _WIN32
    if (MAX_PATH > filePath->size) {
        return E_FN_INVAL_ARRSZ;
    }

    HANDLE hnd = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, pId);
    if (hnd == NULL) {
        return E_GV_SYS_WIN32ERROR;
    }

    auto copiedSz = GetModuleFileNameEx(hnd, NULL, filePath->content, (DWORD)filePath->size);
    filePath->offset = copiedSz;

    if (copiedSz == 0) {
        return E_GV_SYS_WIN32ERROR;
    } else {
        return E_FN_SUCCESS;
    }
#elif __linux
#endif
}

