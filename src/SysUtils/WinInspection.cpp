#include <SysUtils/WinInspection.h>

#if (defined _WIN32)
#include <Windows.h>
#endif

error_t getWindowProc(WHandle hnd, ProcId* pId) {
#if (defined _WIN32)
    auto err = GetWindowThreadProcessId(hnd, pId);

    if (err == 0) {
        return E_GV_SYS_WIN32ERROR;
    } else {
        return E_FN_SUCCESS;
    }
#endif
}
