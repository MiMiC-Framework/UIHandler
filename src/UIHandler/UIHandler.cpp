#include <FUtils/Ret/Either.h>
#include <UIHandler/UITree.h>
#include <UIHandler/UIHandler.h>
#include <UIHandler/Error.h>

// UIAutomation dependencies: Removed in future
#include <UIAEventHandlers/IWOpenHandler.h>
#include <UIAEventHandlers/IWCloseHandler.h>

// CONCEPT
#include <string>
#include <cstring>
#include <functional>
#include <iostream>
#include <atomic>
#include <cstdint>
#include <atomic>
#include <mutex>

static IUIAutomation *UIAutomation;

/**
 * @brief
 *  Initializes the UI Automation interface. Currently Microsoft have two versions:
 *   - CUIAutomation.
 *   - CUIAutomation8.
 *  Further information can be found here:
 *   - https://msdn.microsoft.com/en-us/library/windows/desktop/hh448746(v=vs.85).aspx
 * @return
 *  **True** if if the initialization was succesfull, **false** if not.
 */
bool InitializeUIAutomation() {
    HRESULT hr =
        CoCreateInstance(
            __uuidof(CUIAutomation),
            NULL,
            CLSCTX_INPROC_SERVER ,
            __uuidof(IUIAutomation),
            (void**)&UIAutomation
        );

    return (SUCCEEDED(hr));
}

// ============================================================================
//                              RTS Functions
// ============================================================================

error_t initHRTS(int, char**) {
    if (InitializeUIAutomation()) {
        return E_FN_SUCCESS;
    } else {
        return E_UI_RTS_INIT;
    }
}

error_t closeHRTS() {
    UIAutomation->Release();

    return E_FN_SUCCESS;
}

/**
 * @param uia Object implementing the IUIAutomation interface to be checked.
 * @return Success if the element is initialized, WH_E_RTS_INIT otherwise.
 */
error_t checkRTS(IUIAutomation* uia) {
    if (uia == NULL) {
        return E_UI_RTS_INIT;
    } else {
        return E_FN_SUCCESS;
    }
}

// ============================================================================

// ============================================================================
//                         Event Handler Functions
// ============================================================================

error_t removeAllEventHandlers() {
    if (checkRTS(UIAutomation) != E_FN_SUCCESS) {
        return E_UI_RTS_INIT;
    }

    auto resCode = UIAutomation->RemoveAllEventHandlers();
    if (resCode == S_OK) {
        return E_FN_SUCCESS;
    } else {
        return resCode;
    }
}

error_t removeEventHandler(EVENTID id, IUIAutomationElement* elem, IUIAutomationEventHandler *eHnd) {
    if (checkRTS(UIAutomation) != E_FN_SUCCESS) {
        return E_UI_RTS_INIT;
    }

    auto resCode = UIAutomation->RemoveAutomationEventHandler(id, elem, eHnd);
    if (resCode == S_OK) { return E_FN_SUCCESS; } else { return resCode; };
}

// ============================================================================

// ============================================================================
//                           Elements Functions
// ============================================================================

error_t findElemByRunId(int32_t runtimeId, IUIAutomationElement** targetElem) {
    if (checkRTS(UIAutomation) != E_FN_SUCCESS) {
        return E_UI_RTS_INIT;
    }

    VARIANT prop;
    prop.vt = VT_I4;
    prop.intVal = runtimeId;

    IUIAutomationElement* rootNode = NULL;
    IUIAutomationElementArray* searchElems = NULL;
    IUIAutomationCondition* rIdCond = NULL;

    uierr_t resCode = (uierr_t)E_FN_SUCCESS;

    auto rootRes = UIAutomation->GetRootElement(&rootNode);
    if (FAILED(rootRes) || rootNode == NULL) {
        return E_UI_GET_ROOT_NODE;
    }

    auto condRes = UIAutomation->CreatePropertyCondition(UIA_RuntimeIdPropertyId, prop, &rIdCond);
    if (FAILED(condRes) || rIdCond == NULL) {
        resCode = E_UI_COND_CREATION;
        goto Res;
    }

    auto targetRes = rootNode->FindAll(TreeScope_Descendants, rIdCond, &searchElems);
    if (FAILED(targetRes) || targetElem == NULL) {
        resCode = E_UI_ELEM_NOT_FOUND;
        goto Res;
    }

    int length = 0;
    auto lengthRes = searchElems->get_Length(&length);

    if (FAILED(lengthRes) || length > 1) {
        resCode = E_UI_ELEM_NOT_UNIQUE_ID;
        goto Res;
    }

    auto targetElemRes = searchElems->GetElement(0, targetElem);

    if (targetElemRes == S_OK) {
        resCode = (uierr_t)E_FN_SUCCESS;
        goto Res;
    } else {
        resCode = E_UI_GET_ELEM;
        goto Res;
    }

Res:
    if (rootNode != NULL) {
        rootNode->Release();
    }
    if (rIdCond != NULL) {
        rIdCond->Release();
    }
    if (searchElems != NULL) {
        searchElems->Release();
    }

    return resCode;
}

error_t findElementByName(std::wstring name, IUIAutomationElement* sRoot, enum TreeScope scope, IUIAutomationElement** targetElem) {
    if (checkRTS(UIAutomation) != E_FN_SUCCESS) {
        return E_UI_RTS_INIT;
    }

    VARIANT prop;
    prop.vt = VT_BSTR;
    prop.bstrVal = (wchar_t*)name.c_str();

    IUIAutomationElementArray* searchElems = NULL;
    IUIAutomationCondition* rIdCond = NULL;

    HRESULT resCode;

    auto condRes = UIAutomation->CreatePropertyCondition(UIA_NamePropertyId, prop, &rIdCond);

    if (FAILED(condRes) || rIdCond == NULL) {
        resCode = condRes;
        goto Res;
    }

    auto targetRes = sRoot->FindAll(scope, rIdCond, &searchElems);

    if (FAILED(targetRes) || targetElem == NULL) {
        resCode = condRes;
        goto Res;
    } else {
        resCode = E_FN_SUCCESS;
        goto Res;
    }

Res:
    if (rIdCond != NULL) {
        rIdCond->Release();
    }
    if (searchElems != NULL) {
        searchElems->Release();
    }

    return resCode;
}

// ============================================================================

// ============================================================================
//                        Helper Window Functions
// ============================================================================

struct handle_data {
    unsigned long process_id;
    HWND best_handle;
};

bool isMainWindow(HWND handle)
{
    return GetWindow(handle, GW_OWNER) == (HWND)0 && IsWindowVisible(handle);
}

BOOL CALLBACK windowsProcCallback(HWND handle, LPARAM lParam)
{
    handle_data& data = *(handle_data*)lParam;
    unsigned long process_id = 0;

    GetWindowThreadProcessId(handle, &process_id);

    if (data.process_id != process_id || !isMainWindow(handle)) {
        return TRUE;
    }

    data.best_handle = handle;
    return FALSE;
}

HWND findWindowByProcessId(unsigned long pId)
{
    handle_data data;
    data.process_id = pId;
    data.best_handle = NULL;

    HANDLE hnd = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, pId);

    if (hnd == NULL) {
        return NULL;
    }

    EnumWindows(windowsProcCallback, (LPARAM)&data);

    return data.best_handle;
}

error_t findWindowUIElemByHwnd(HWND handle, IUIAutomationElement** window) {
    if (checkRTS(UIAutomation) != E_FN_SUCCESS) {
        return E_UI_RTS_INIT;
    }

    VARIANT prop;
    prop.vt = VT_I4;
    // This can be seeing faulty, but it's stated in Microsoft documentation that
    // Windows handles are 32 bits integers:
    // https://msdn.microsoft.com/en-us/library/windows/desktop/ee684017(v=vs.85).aspx
    prop.intVal = (int32_t)handle;

    IUIAutomationElementArray* searchElems = NULL;
    IUIAutomationCondition* rIdCond = NULL;
    IUIAutomationElement* sRoot = NULL;

    HRESULT resCode = E_FN_SUCCESS;

    auto condRes = UIAutomation->CreatePropertyCondition(UIA_NativeWindowHandlePropertyId, prop, &rIdCond);

    if (FAILED(condRes) || rIdCond == NULL) {
        resCode = condRes;
        goto Res;
    }

    auto rootRes = UIAutomation->GetRootElement(&sRoot);
    if (FAILED(rootRes) || sRoot == NULL) {
        return E_UI_GET_ROOT_NODE;
    }

    auto targetRes = sRoot->FindAll(TreeScope_Children, rIdCond, &searchElems);

    if (FAILED(targetRes) || sRoot == NULL) {
        resCode = condRes;
        goto Res;
    }

    auto targetElemRes = searchElems->GetElement(0, window);

    if (targetElemRes == S_OK) {
        resCode = E_FN_SUCCESS;
        goto Res;
    } else {
        resCode = E_UI_GET_ELEM;
        goto Res;
    }

Res:
    if (sRoot != NULL) {
        sRoot->Release();
    }
    if (rIdCond != NULL) {
        rIdCond->Release();
    }
    if (searchElems != NULL) {
        searchElems->Release();
    }

    return resCode;
}

error_t findWindow(AppInfo appInfo, IUIAutomationElement** window) {
    // Declare pointers prior to nex operations for failing case.
    ProcId* pIds = NULL;
    LPTSTR pBinPathData = NULL;

    pIds = (ProcId*)malloc(sizeof(ProcId)*ST_RUNNING_PROCS);
    ProcId_A pIds_A { pIds, ST_RUNNING_PROCS, 0 };
    int resCode = E_FN_SUCCESS;

    auto rProcsRes = getRunningProcs(&pIds_A);

    if (rProcsRes != E_FN_SUCCESS) {
        resCode = rProcsRes;
        goto Res;
    }

    pBinPathData = (TCHAR*)malloc(sizeof(TCHAR)*MAX_PATH);
    TChar_A pBinPath { pBinPathData, MAX_PATH };

    ProcId rPid = NULL;
    HWND hwnd = NULL;

    for (int i = 0; i < ST_RUNNING_PROCS; i++) {
        auto crrPid = pIds_A.content[i];
        auto pBinPathErrCode = getProcBinPath(crrPid, &pBinPath);

        if (strcmp(appInfo.path.c_str(), pBinPath.content) == 0) {
            hwnd = findWindowByProcessId(crrPid);
            break;
        }
    }

    if (hwnd == NULL) {
        resCode = E_FN_INVAL;
        goto Res;
    }

    auto sByHwndRes = findWindowUIElemByHwnd(hwnd, window);

    if (sByHwndRes != E_FN_SUCCESS) {
        resCode = sByHwndRes;
    } else {
        resCode = E_FN_SUCCESS;
    }

    goto Res;
Res:
    if (pIds != NULL) {
        free(pIds);
    }
    if (pBinPathData != NULL) {
        free(pBinPathData);
    }

    return resCode;
}

// ============================================================================

// ============================================================================
//                      App Window Detection Functions
// ============================================================================

error_t detectWindowOpening(
    AppInfo             appInfo,
    uint64_t            eventId,
    SVar<UIEventQueue>& sUIEventQueue,
    SVar<LogQueue>&     sLogQueue,
    EventHndResrcs&     resrcs)
{
    if (checkRTS(UIAutomation) != E_FN_SUCCESS) {
        return E_UI_RTS_INIT;
    }

    IUIAutomationElement* rootNode = NULL;

    auto rootRes = UIAutomation->GetRootElement(&rootNode);
    if (FAILED(rootRes) || rootNode == NULL) {
        return E_UI_GET_ROOT_NODE;
    }

    IUIAutomationCacheRequest* cache = NULL;
    UIAutomation->CreateCacheRequest(&cache);
    cache->AddProperty(UIA_ProcessIdPropertyId);
    cache->AddProperty(UIA_NativeWindowHandlePropertyId);
    cache->AddProperty(UIA_NamePropertyId);
    cache->put_AutomationElementMode(AutomationElementMode_None);

    auto eventHandler = new OpenWindowHandler(appInfo, eventId, sUIEventQueue, sLogQueue);

    resrcs.handler  = eventHandler;
    resrcs.rootElem = rootNode;
    resrcs.eId      = UIA_Window_WindowOpenedEventId;

    UIAutomation->AddAutomationEventHandler(
        UIA_Window_WindowOpenedEventId,
        rootNode,
        TreeScope_Children,
        cache,
        eventHandler
    );

    return E_FN_SUCCESS;
}

error_t detectWindowClosing(
    AppInfo             appInfo,
    uint64_t            eventId,
    SVar<UIEventQueue>& sUIEventQueue,
    SVar<LogQueue>&     sLogQueue,
    EventHndResrcs&     resrcs)
{
    if (checkRTS(UIAutomation) != E_FN_SUCCESS) {
        return E_UI_RTS_INIT;
    }

    IUIAutomationElement* rootNode = NULL;
    IUIAutomationElementArray* searchElems = NULL;
    IUIAutomationCondition* rIdCond = NULL;

    auto rootRes = UIAutomation->GetRootElement(&rootNode);
    if (FAILED(rootRes) || rootNode == NULL) {
        return E_UI_GET_ROOT_NODE;
    }

    IUIAutomationCacheRequest* cache = NULL;
    UIAutomation->CreateCacheRequest(&cache);
    cache->AddProperty(UIA_ProcessIdPropertyId);
    cache->AddProperty(UIA_NativeWindowHandlePropertyId);
    cache->put_AutomationElementMode(AutomationElementMode_None);


    auto eventHandler = new CloseWindowHandler(appInfo, eventId, sUIEventQueue, sLogQueue);

    resrcs.handler  = eventHandler;
    resrcs.rootElem = rootNode;
    resrcs.eId      = UIA_Window_WindowClosedEventId;

    UIAutomation->AddAutomationEventHandler(
        UIA_Window_WindowClosedEventId,
        rootNode,
        TreeScope_Children,
        cache,
        eventHandler
    );

    return E_FN_SUCCESS;
}

// ============================================================================

// ============================================================================
//                    Element Changes Detection Functions
// ============================================================================

class ChildrenRemovalDetector : public IUIAutomationStructureChangedEventHandler {
private:
    LONG                    _refCount;
    SVar<UIEventQueue>&     _sUIEventQueue;
    SVar<LogQueue>&         _sLogQueue;
    IUIAutomationElement*   _targetChild;

public:
    // Constructor.
    ChildrenRemovalDetector(
        IUIAutomationElement* targetChild,
        SVar<UIEventQueue>& sUIEventQueue,
        SVar<LogQueue>& sLogQueue
    ) : _refCount(1),
        _targetChild(targetChild),
        _sUIEventQueue(sUIEventQueue),
        _sLogQueue(sLogQueue)
    {}

    // IUnknown methods.
    ULONG STDMETHODCALLTYPE AddRef() {
        ULONG ret = InterlockedIncrement(&_refCount);
        return ret;
    }

    ULONG STDMETHODCALLTYPE Release() {
        ULONG ret = InterlockedDecrement(&_refCount);
        if (ret == 0) {
            delete this;
            return 0;
        }
        return ret;
    }

    HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void** ppInterface)
    {
        if (riid == __uuidof(IUnknown)) {
            *ppInterface = static_cast<IUIAutomationStructureChangedEventHandler*>(this);
        } else if (riid == __uuidof(IUIAutomationStructureChangedEventHandler)) {
            *ppInterface=static_cast<IUIAutomationStructureChangedEventHandler*>(this);
        } else {
            *ppInterface = NULL;
            return E_NOINTERFACE;
        }
        this->AddRef();
        return S_OK;
    }

    // IUIAutomationStructureChangedEventHandler methods
    HRESULT STDMETHODCALLTYPE HandleStructureChangedEvent(
        IUIAutomationElement* pSender,
        StructureChangeType changeType,
        SAFEARRAY* pRuntimeID
    ) {
        auto childrenModification =
            (changeType == StructureChangeType_ChildRemoved)        |
            (changeType == StructureChangeType_ChildrenBulkRemoved) |
            (changeType == StructureChangeType_ChildrenInvalidated);

        if (!childrenModification) {
            return S_OK;
        }

        //          TODO: Should and error count as invalidation?
        // ====================================================================
        // Need to check how to API behave in that cases, maybe doing that we
        // get duplicate results from elements in the chain that detect it
        // correctly.
        // ====================================================================

        IUIAutomationElementArray* childs = NULL;
        auto childsRes =  pSender->GetCachedChildren(&childs);
        if (childsRes != S_OK) { return childsRes; }

        int childNum = 0;
        auto childNumRes = childs->get_Length(&childNum);
        if (childNumRes != S_OK) { return childNumRes; }

        // ====================================================================

        bool childPresent = FALSE;

        for (int i = 0; i < childNum; i++) {
            IUIAutomationElement* iElem = NULL;
            BOOL isSame = FALSE;

            auto childRes = childs->GetElement(i, &iElem);
            if (childRes != S_OK) { continue; }
            auto cmpElemsRes = UIAutomation->CompareElements(_targetChild, iElem, &isSame);

            if (cmpElemsRes == S_OK && isSame == TRUE) {
                childPresent = TRUE;
                break;
            }
        }

        if (childPresent == FALSE) {
            // Comunicate back the result: TODO
            // ================================================================
            // std::unique_lock<std::mutex> lock(_m);

            // _modified = TRUE;

            // lock.unlock();
            // _cv.notify_all();
            // ================================================================
        }

        return S_OK;
    }
};

error_t detectStructuralChange(IUIAutomationStructureChangedEventHandler* eventHnd, IUIAutomationElement* target) {
    auto addStHandlerRes = UIAutomation->AddStructureChangedEventHandler(
        target,
        TreeScope_Element,
        NULL,
        eventHnd
    );

    return S_OK;
}

error_t detectElementInvalidation(
    IUIAutomationElement*       wRoot,
    IUIAutomationElement*       targetElem,
    SVar<UIEventQueue>&         sUIEventQueue,
    SVar<LogQueue>&             sLogQueue,
    std::list<EventHndResrcs>&  resrces
) {
    if (checkRTS(UIAutomation) != E_FN_SUCCESS) {
        return E_UI_RTS_INIT;
    }

    IUIAutomationElement* rootElem = NULL;
    auto rootElemRes = UIAutomation->GetRootElement(&rootElem);
    if (rootElemRes != S_OK) {
        return rootElemRes;
    }

    IUIAutomationCacheRequest* stCache = NULL;

    UIAutomation->CreateCacheRequest(&stCache);
    stCache->put_TreeScope(TreeScope_Subtree);
    stCache->put_AutomationElementMode(AutomationElementMode_None);

    IUIAutomationCondition* rawCond = NULL;
    auto rawCondRes = UIAutomation->get_RawViewCondition(&rawCond);
    if (rawCondRes != S_OK) {
        return rawCondRes;
    }

    IUIAutomationTreeWalker* tWalker = NULL;
    auto tWalkerRes = UIAutomation->CreateTreeWalker(rawCond, &tWalker);
    if (tWalkerRes != S_OK) {
        return tWalkerRes;
    }

    IUIAutomationElement* parent = NULL;
    IUIAutomationElement* currentElem = targetElem;
    BOOL same = FALSE;
    auto cmpRes = UIAutomation->CompareElements(parent, wRoot, &same);
    if (cmpRes != S_OK) {
        /* TODO: Search for better error code */
        return E_FN_ABORTED;
    }

    while (!same) {
        tWalker->GetParentElement(currentElem, &parent);

        BOOL isRoot;
        UIAutomation->CompareElements(currentElem, rootElem, &isRoot);

        // Check if parameter is invalid because non father element is supplied as WRoot.
        if (isRoot) {
            return E_FN_INVAL;
        }

        auto childrenRmDetect = new ChildrenRemovalDetector(targetElem, sUIEventQueue, sLogQueue);
        auto addStHandlerRes = UIAutomation->AddStructureChangedEventHandler(
            parent,
            TreeScope_Children,
            stCache,
            childrenRmDetect
        );

        if (addStHandlerRes != S_OK) {
            return addStHandlerRes;
        }

        // Next iteration
        cmpRes = UIAutomation->CompareElements(parent, wRoot, &same);
        if (cmpRes != S_OK) {
            /* TODO: Search for better error code */
            return E_FN_ABORTED;
        }
    }

    return E_FN_SUCCESS;
}

// ============================================================================

// ============================================================================
//                         Invoke Elements Functions
// ============================================================================

error_t triggerElementByName(std::wstring name, IUIAutomationElement* sRoot, struct UIElemAction action) {
    if (checkRTS(UIAutomation) != E_FN_SUCCESS) {
        return E_UI_RTS_INIT;
    }

    IUIAutomationElement* sElem = NULL;
    auto findError = findElementByName(name, sRoot, TreeScope_Descendants, &sElem);

    if (findError != E_FN_SUCCESS) {
        return findError;
    }

    if (action.code == Action_Invoke) {
        IUIAutomationInvokePattern* invokePattern = NULL;
        auto iPatternRes = sElem->GetCurrentPattern(UIA_InvokePatternId, (IUnknown**)&invokePattern);

        if (iPatternRes != S_OK) {
            return iPatternRes;
        }

        invokePattern->Invoke();
    } else if (action.code == Action_ToogleExpandCollapse) {
        IUIAutomationExpandCollapsePattern* expandCollapse = NULL;
        auto eCPatternRes = sElem->GetCurrentPattern(UIA_ExpandCollapseExpandCollapseStatePropertyId, (IUnknown**)&expandCollapse);
        if (eCPatternRes != S_OK) {
            return eCPatternRes;
        }

        VARIANT eCState;
        eCState.vt = VT_I4;

        auto eCPropertyRes = sElem->GetCurrentPropertyValue(UIA_ExpandCollapseExpandCollapseStatePropertyId, &eCState);
        if (eCPropertyRes != S_OK) {
            return eCPropertyRes;
        }

        if (eCState.intVal == ExpandCollapseState_Collapsed) {
            expandCollapse->Expand();
        } else {
            expandCollapse->Collapse();
        }
    } else {
        return E_FN_INVAL;
    }

    return E_FN_SUCCESS;
}

// ============================================================================

/*
////////////////////////////////////////////////////////////////////////////////////
                TODO: FUTURE SECTION FOR AUTOMATIC LANGUAGE MAP
////////////////////////////////////////////////////////////////////////////////////

error_t getUniqueProperties(IUIAutomationElement* elem) {
    throw "WIP";
}

error_t findUniqueShape(int32_t runtimeId, Shape* shape) {
    IUIAutomationElement* targetElem = NULL;

    auto fRes = findElemByRunId(runtimeId, &targetElem);

    if (fRes != Success) {
        return fRes;
    }

    checkUniqueProperties()
    IUIAutomationTreeWalker* treeWalker = NULL;
    UIAutomation->get_ContentViewWalker(&treeWalker);

    IUIAutomationElement* pSibling = NULL;
    treeWalker->GetPreviousSiblingElement(targetElem, &pSibling);
}

////////////////////////////////////////////////////////////////////////////////////
*/
