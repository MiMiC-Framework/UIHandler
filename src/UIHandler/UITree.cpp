#include <UIHandler/UITree.h>

struct CtrlCmp {
    const char* name;
    const struct WProps* props;
};

struct ContCmp {
    const char* name;
    const struct WProps* props;
};

struct CtrlTreeCmp {
    const struct CtrlCmp* cmp;
    const struct CtrlCmpTree** childs;
};

struct ContTreeCmp {
    const struct ContCmp* cmp;
    const struct ContCmpTree** childs;
};

struct RawCmp {
    union {
        struct CtrlCmp ctlrCmp;
        struct ContCmp contCmp;
    };
    enum CmpType;
};

struct RawCmpTree {
    union {
        const struct CtrlTreeCmp* ctrlTree;
        const struct ContTreeCmp* contTree;
    };
    const enum CmpType cmpType;
    const struct RawCmpTree** childs;
};

struct UITree {
    union {
        const struct CtrlCmpTree* ctlrTree;
        const struct ContCmpTree* contTree;
        const struct RawCmpTree* rawTree;
    };
    const enum TreeType type;
    struct CmpTree* tree;

    const char* name;
};
