#include <UIAEventHandlers/IWOpenHandler.h>
#include <UIHandler/UITypes.h>

// OpenWindowHandler Constructor.
OpenWindowHandler::OpenWindowHandler(
    AppInfo             appInfo,
    uint64_t            eventId,
    SVar<UIEventQueue>& uiEventQueue,
    SVar<LogQueue>&     logQueue
) :
    _refCount(1),
    _appInfo(appInfo),
    _eventId(eventId),
    _uiEventQueue(uiEventQueue),
    _logQueue(logQueue)
{}

// IUnknown methods.
ULONG STDMETHODCALLTYPE OpenWindowHandler::AddRef() {
    ULONG ret = InterlockedIncrement(&_refCount);
    return ret;
}

ULONG STDMETHODCALLTYPE OpenWindowHandler::Release() {
    ULONG ret = InterlockedDecrement(&_refCount);
    if (ret == 0) {
        delete this;
        return 0;
    }
    return ret;
}

ULONG STDMETHODCALLTYPE OpenWindowHandler::OpenWindowRelease() {
    ULONG ret = InterlockedDecrement(&_refCount);
    if (ret == 0) {
        delete this;
        return 0;
    }
    return ret;
}

HRESULT STDMETHODCALLTYPE OpenWindowHandler::QueryInterface(REFIID riid, void** ppInterface)  {
    if (riid == __uuidof(IUnknown)) {
        *ppInterface=static_cast<IUIAutomationEventHandler*>(this);
    } else if (riid == __uuidof(IUIAutomationEventHandler)) {
        *ppInterface=static_cast<IUIAutomationEventHandler*>(this);
    } else {
        *ppInterface = NULL;
        return E_NOINTERFACE;
    }

    this->AddRef();
    return S_OK;
}

    // IUIAutomationEventHandler methods
HRESULT STDMETHODCALLTYPE OpenWindowHandler::HandleAutomationEvent(IUIAutomationElement * pSender, EVENTID eventID) {
    if (eventID != UIA_Window_WindowOpenedEventId) {
        return S_OK;
    }

    HRESULT errCode = 0;
    int pId = 0;

    try {
        auto pIdRes = pSender->get_CachedProcessId(&pId);
        if (pIdRes != S_OK || pId == NULL) {
                errCode = pIdRes;
                goto Err;
        }

        TChar_A pBinPath_A;
        pBinPath_A.content = (TCHAR*)malloc(MAX_PATH);
        pBinPath_A.size = MAX_PATH;
        pBinPath_A.offset = 0;

        auto pBinErr = getProcBinPath(pId, &pBinPath_A);
        if (pBinErr != E_FN_SUCCESS || pBinPath_A.content == NULL) {
            free(pBinPath_A.content);
            errCode = pBinErr;
            goto Err;
        }

        // Create safe type and free memory.
        // ============================================================
        String pBinPathStr { pBinPath_A.content };
        free(pBinPath_A.content);
        // ============================================================

        if (pBinPathStr != _appInfo.path) {
            goto Err;
        }

        UIA_HWND wHandle;
        auto windowHandleRes = pSender->get_CachedNativeWindowHandle(&wHandle);

        if (windowHandleRes != S_OK) {
            errCode = windowHandleRes;
            goto Err;
        }

        BSTR eName = NULL;
        auto gnerr = pSender->get_CachedName(&eName);

        if (gnerr != S_OK || eName == NULL) {
            errCode = gnerr;
            goto Err;
        }

        std::wstring wName(eName, SysStringLen(eName));
        Variant pl;
        pl.type  = VT_PVoid;
        pl.pVoid = wHandle;

        UIEvent eventPayload {
            _eventId,
            _appInfo.appId,
            UIEventWinOpened,
            pl,
            wName,
            pSender,
            E_FN_SUCCESS
        };

        // Comunicate back the result
        // ============================================================
        updSVar(_uiEventQueue,
            [] (std::queue<UIEvent>& appUIEventQueue, UIEvent&& uiEvent) {
                appUIEventQueue.push(std::move(uiEvent));
            },
            std::move(eventPayload)
        );
        // ============================================================
    } catch(std::exception ex) {
        Variant pl;
        pl.type  = VT_PVoid;
        pl.pVoid = NULL;

        UIEvent failedEvent { _eventId, _appInfo.appId, UIEventWinOpened, pl, L"", NULL, E_UI_GET_ELEM };

        // ============================================================
        // Comunicate back the error
        // ============================================================
        updSVar(_uiEventQueue,
            [] (std::queue<UIEvent>& appUIEventQueue, UIEvent&& uiEvent) {
                appUIEventQueue.push(uiEvent);
            },
            std::move(failedEvent)
        );
        // ============================================================

        // Log the error
        // ============================================================
        updSVar(_logQueue,
            [] (LogQueue& appUIEventQueue, std::string&& logMsg) {
                appUIEventQueue.push(std::move(logMsg));
            },
            std::move(std::string(ex.what()))
        );
        // ============================================================
    }

    return S_OK;

Err:
     Variant pl;
     pl.type  = VT_PVoid;
     pl.pVoid = NULL;

     UIEvent failedEvent { _eventId, _appInfo.appId, UIEventWinOpened, pl, L"", NULL, errCode };

     // ============================================================
     // Comunicate back the error
     // ============================================================
     updSVar(_uiEventQueue,
         [] (std::queue<UIEvent>& appUIEventQueue, UIEvent&& uiEvent) {
             appUIEventQueue.push(uiEvent);
         },
         std::move(failedEvent)
     );
     // ============================================================

     // Log the error
     // ============================================================
     std::string errMsg = "UIEventWinOpened: Event detection failed";

     updSVar(_logQueue,
         [] (LogQueue& appUIEventQueue, std::string&& logMsg) {
             appUIEventQueue.push(std::move(logMsg));
         },
         std::move(errMsg)
     );
     // ============================================================

     return errCode;
}
