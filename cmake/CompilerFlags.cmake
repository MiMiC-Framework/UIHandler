Function(ClangFlags FLAGS)
    If (CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
        If ({CMAKE_BUILD_TYPE STREQUAL "Debug")
           Set(${FLAGS} "${${FLAGS}} -std=c++1z -O0 -fexceptions -fmodules -fcxx-modules -Werror \
                         -Wall -Wfatal-errors -Wextra -fdiagnostics-color=always" PARENT_SCOPE)
        ElseIf (CMAKE_BUILD_TYPE STREQUAL "Release")
           Set(${FLAGS} "${${FLAGS}} -std=c++1z -O3 -fmodules -fcxx-modules -Werror \
                         -Wall -Wfatal-errors -Wextra -fdiagnostics-color=always" PARENT_SCOPE)
        EndIf()
    EndIf()
EndFunction()

Function(GccFlags FLAGS)
    If (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
        If (CMAKE_BUILD_TYPE STREQUAL "Debug")
            Set(${FLAGS} "${${FLAGS}} -std=c++1z -O0 -fexceptions -g -pedantic -fconcepts \
                          -Wall -Werror -Wfatal-errors -Wextra -fdiagnostics-color=always" PARENT_SCOPE)
        ElseIf (CMAKE_BUILD_TYPE STREQUAL "Release")
            Set(${FLAGS} "${${FLAGS}} -std=c++1z -O0 -g -pedantic -fconcepts \
                          -Wall -Werror -Wfatal-errors -Wextra -fdiagnostics-color=always" PARENT_SCOPE)
        EndIf()
    EndIf()
EndFunction()

Function(MSVCFlags FLAGS)
    If (CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")
        If (CMAKE_BUILD_TYPE STREQUAL "Debug")
            Set(${FLAGS} "${${FLAGS}} -std:c++latest /Od /Zi /W4 /EHsc /MDd -DGTEST_LANG_CXX11=1" PARENT_SCOPE)
        ElseIf (CMAKE_BUILD_TYPE STREQUAL "Release")
            Set(${FLAGS} "${${FLAGS}} -std:c++latest /Ox /W4 /EHsc -DGTEST_LANG_CXX11=1" PARENT_SCOPE)
        EndIf()
    EndIf()
EndFunction()

Function(IntelFlags)
    If ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Intel")
        MESSAGE(FATAL_ERROR "Not supported compiler")
    EndIf()
EndFunction()

Function(SetCompilerSpecificFlags SPECIFIC_FLAGS)
    ClangFlags(${SPECIFIC_FLAGS})
    GccFlags(${SPECIFIC_FLAGS})
    MSVCFlags(${SPECIFIC_FLAGS})
    IntelFlags()
    Set(${SPECIFIC_FLAGS} "${${SPECIFIC_FLAGS}}" PARENT_SCOPE)
EndFunction()

Function(SET_COMPILER_FLAGS COMPILER_FLAGS)
    SetCompilerSpecificFlags(CXX_SPECIFIC_FLAGS)
    Set(${COMPILER_FLAGS} "${CXX_GLOBAL_FLAGS} ${CXX_SPECIFIC_FLAGS}" PARENT_SCOPE)
EndFunction()

SET_COMPILER_FLAGS(CMAKE_CXX_FLAGS)
