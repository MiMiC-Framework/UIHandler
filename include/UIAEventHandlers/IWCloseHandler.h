#ifndef UI_ICLOSEWINDOWHANDLER_H
#define UI_ICLOSEWINDOWHANDLER_H

#include <UIHandler/UITypes.h>
#include <UIHandler/UIHandler.h>
#include <FUtils/Sync/SVar.h>

#include <mutex>
#include <thread>
#include <queue>

#include <UIAutomation.h>

using LogQueue = std::queue<std::string>;

#ifndef UNICODE
  typedef std::string String;
#else
  typedef std::wstring String;
#endif

class CloseWindowHandler : public IUIAutomationEventHandler {
private:
    LONG                    _refCount;
    AppInfo                 _appInfo;
    uint64_t                _eventId;
    SVar<LogQueue>&         _logQueue;
    SVar<UIEventQueue>&     _uiEventQueue;

public:
    // Constructor.
    CloseWindowHandler(AppInfo _appInfo, uint64_t _eventId, SVar<UIEventQueue>& _uiEventQueue, SVar<LogQueue>& _logQueue);
    ULONG STDMETHODCALLTYPE CloseWindowHandler::AddRef();
    ULONG STDMETHODCALLTYPE CloseWindowHandler::Release();
    ULONG STDMETHODCALLTYPE CloseWindowHandler::OpenWindowRelease();
    HRESULT STDMETHODCALLTYPE CloseWindowHandler::QueryInterface(REFIID riid, void** ppInterface);
    HRESULT STDMETHODCALLTYPE CloseWindowHandler::HandleAutomationEvent(IUIAutomationElement * pSender, EVENTID eventID);
};

#endif
