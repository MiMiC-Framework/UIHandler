#ifndef UIH_IOPENWINDOWHANDLER_H
#define UIH_IOPENWINDOWHANDLER_H

#include <UIHandler/UITypes.h>
#include <UIHandler/UIHandler.h>
#include <FUtils/Sync/SVar.h>


#include <mutex>
#include <thread>
#include <queue>

#include <UIAutomation.h>

using LogQueue = std::queue<std::string>;

#ifndef UNICODE
  typedef std::string String;
#else
  typedef std::wstring String;
#endif

/**
 * @brief
 *  Class for handling windows openning events.
 * @details
 *  This class is used for notifying when a new windows of a tracked application has been openned.
 *
 *  Note:
 *  Events will be consumed by the UIService.
 */
class OpenWindowHandler : public IUIAutomationEventHandler {
private:
    /**
     * @brief _refCount
     *  Reference counting for freeing Handler resources.
     */
    LONG                    _refCount;
    /**
     * @brief _appInfo
     *  Information about the application that wants to be detected.
     */
    AppInfo                 _appInfo;
    /**
     * @brief _eventId
     *  Event identification for matching the event with the proper UI Action.
     */
    uint64_t                _eventId;
    /**
     * @brief _logQueue
     *  Logging queue for sending information to the logging service.
     */
    SVar<LogQueue>&         _logQueue;
    /**
     * @brief _uiEventQueue
     *  Event queue for sending received UI Events that match the desired application to the UI
     *  Service.
     */
    SVar<UIEventQueue>&     _uiEventQueue;

public:
    /**
     * @brief Constructor
     */
    OpenWindowHandler(AppInfo _appInfo, uint64_t _eventId, SVar<UIEventQueue>& _uiEventQueue, SVar<LogQueue>& _logQueue);
    ULONG STDMETHODCALLTYPE OpenWindowHandler::AddRef();
    ULONG STDMETHODCALLTYPE OpenWindowHandler::Release();
    ULONG STDMETHODCALLTYPE OpenWindowHandler::OpenWindowRelease();
    HRESULT STDMETHODCALLTYPE OpenWindowHandler::QueryInterface(REFIID riid, void** ppInterface);
    HRESULT STDMETHODCALLTYPE OpenWindowHandler::HandleAutomationEvent(IUIAutomationElement * pSender, EVENTID eventID);
};

#endif
