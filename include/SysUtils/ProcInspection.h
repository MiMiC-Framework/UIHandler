#ifndef PINSPECTION_H
#define PINSPECTION_H

#include <UIHandler/Error.h>

#include <vector>

#ifdef _WIN32
#include <windows.h>
#elif __linux
#include <sys/types.h>
#endif

extern "C" {

/**
 * @typedef ProcHandle
 * @brief This typedef maps a platform specific "Process Handle" type to ours.
 */
#ifdef _WIN32
typedef HANDLE ProcHandle;
typedef DWORD ProcId;
#elif __linux
typedef pid_t ProcHandle;
// TODO: Check the actual type of procId in Linux.
typedef pid_t ProcId;
#endif

/**
 * Path size to be used as a start value. If paths are bigger
 * than this side, then functions should allocate the extra space
 * for the buffer.
 */
#ifdef _WIN32
#define ST_PATH_SIZE MAX_PATH
#elif __linux
#define ST_PATH_SIZE 260
#endif

#define ST_PROC_NAME_SZ uint32_t(512)
#define ST_RUNNING_PROCS uint32_t(1024)

/**
 * @brief
 * Struct that represent an array of ProcId.
 */
struct ProcId_A {
    ProcId* content;
    size_t size;
    size_t offset;
};

/**
 * @brief
 * Struct for holding a process name.
 */
struct Char_A {
    char* content;
    size_t size;
    size_t offset;
};

/**
 * @brief
 * Struct for holding a process name.
 */
struct TChar_A {
    LPTSTR content;
    size_t size;
    size_t offset;
};

/**
 * @brief
 * Returns a array with the ProcId of the running processes.
 *
 * @param procIds
 * The pointer to the allocated memory area of the array to fill with
 * the running processes id.
 * @return
 * A error code or 0, if everithing goes as expected.
 */
error_t getRunningProcs(struct ProcId_A* pIds);

/**
 * @brief
 * Return the name of a process based on its pid.
 */
error_t getProcName(ProcId pId, struct Char_A* name);

/**
 * @brief
 * Return an array of process pIds whose names match with the given string.
 */
error_t findProcByName(struct Char_A* name, struct ProcId_A* pId);

/**
 * @brief
 * Returns the filepath of the provided process handle.
 *
 * @param proc
 * The process handle which is going to be inspected.
 */
error_t getProcBinPath(ProcId pId, struct TChar_A* filePath);

}

#endif
