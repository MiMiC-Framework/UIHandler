#ifndef UIH_WINSPECTION_H
#define UIH_WINSPECTION_H

#include <stdint.h>

#include <UIHandler/Error.h>
#include <SysUtils/ProcInspection.h>

#ifdef _WIN32
#include <windows.h>
#elif __linux
#include <X11/X.h>
#endif

/**
 * @typedef WHandle
 *
 * @brief This typedef maps a platform specific "Window Handle" type to ours.
 */
#ifdef _WIN32
typedef HWND WHandle;
#elif __linux
typedef Window WHandle;
#endif

extern "C" {

/**
 * @struct WSize
 *
 * @brief
 * Struct that represents the actual Window size.
 */
struct WSize {
    const uint16_t height;
    const uint16_t weight;
};

/**
 * @struct WProps
 *
 * @brief
 * Struct that holds the properties of this Window. This info could contains:
 *     + Backend technology.
 *     + Size/Window position.
 *     + Process is associated with this Windows handle.
 *     + Etc...
 *
 * All the information needed to fully identify the Window, and being able
 * to interact with it.
 */
struct WProps {
    ProcHandle hnd;
};

/**
 * @brief
 * Returns the process handle associated with this window.
 * @param hnd
 * The window handle from which the process handle is requested.
 * @param[out] pId
 * Output parameter for returning the process handle.
 * @return
 * EF_SUCCESS if everything was OK, error in other case.
 */
error_t getWindowProc(WHandle hnd, ProcId* pId);

/**
 * @brief getWindowProps
 * @param hnd
 * @param[out] props
 * Output parameter with UI properties associated with a Window handle.
 * @return
 */
error_t getWindowProps(WHandle hnd, struct WProps_A* props);

}

#endif
