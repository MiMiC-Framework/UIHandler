#ifndef UIH_UIHANDLER_H
#define UIH_UIHANDLER_H

#include <UIHandler/UITypes.h>
#include <UIHandler/UITree.h>

#include <UIHandler/Error.h>
#include <SysUtils/WinInspection.h>

#include <FUtils/Sync/SVar.h>
#include <FUtils/Ret/Either.h>

// Standard
#include <stdint.h>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include <queue>

// Windows deps
#include <UIAutomation.h>
#include <Windows.h>

using LogQueue = std::queue<std::string>;

/**
 * @brief initHRTS
 *  Init the handler RTS.
 * @return
 *  EF_SUCCESS if everything was OK, error in other case.
 */
error_t initHRTS(int argc, char* argv[]);

/**
 * @brief closeHRTS
 *  Close the handler RTS.
 * @return
 *  EF_SUCCESS if everything was OK, error in other case.
 */
error_t closeHRTS();

/**
 * @brief
 *  Convenience function for removing all created event handlers || UIA
 */
error_t removeAllEventHandlers();

/**
 * @brief
 *  Removes a particular event handler from UIAElement.
 */
error_t removeEventHandler(EVENTID id, IUIAutomationElement* elem, IUIAutomationEventHandler *eHnd);

/**
 * @brief
 *  Type for storing resources associated with Handlers.
 */
struct EventHndResrcs {
    IUIAutomationEventHandler*  handler;
    IUIAutomationElement*       rootElem;
    EVENTID                     eId;
};

/**
 * @brief
 *  Struct that holds an array of raw componentes.
 */
struct RawCmp_A {
    RawCmp*     data;
    size_t      size;
    size_t      offset;
};

/**
 * @brief
 *  Function to release all the memory that was created by the Handler.
 *
 *  <b>CERT Breaking!</b><br>
 *  This function breaks the MEM00-C rule from the CERT coding standard, but
 *  there are several justifications for this:
 *      - The amount of memory that is going to be used by many functions in
 *     the library is very variable and unpredictable.
 *      - Many functions in the library can't pre-know the size of the memory
 *        they are going to need, so they cannot check and return asking for
 *     more memory until they have computed the results.
 *      - Functions involving many UI elements could have high computational
 *        cost, and recomputing is not an option.
 * @param data
 *  Pointer to the memory to be released.
 * @return
 *  EF_SUCCESS if everything was OK, error in other case.
 */
error_t pFree(void* data);

/**
 * @brief
 *  Returns the tree representing the actual state of the Windows UI.
 *
 * @param hnd
 *  The window handle of the window which UI Tree is going to be
 *  retrieved.
 */
error_t getWindowTreeRoot(WHandle hnd, UITree* treeRoot);

/**
 * @brief findComponent
 *  Returns a list of components matching with the specified rules.
 * @param property
 *  Properties that needs to match for elements to be returned.
 * @param val
 *  Values that needs to match for elements to be returned.
 * @param cmps
 *  Array of the components to be returned
 * @return
 *  EF_SUCCESS if everything was OK, error in other case.
 */
error_t findComps(struct UIElemProperty property, struct Variant val, struct RawCmp_A* cmps);

enum Move {
    M_LeftSibling,
    M_RightSibling,
    M_Father,
    M_FirstSon
};

struct A_Move {
    enum Move* data;
    size_t size;
    size_t offset;
};

struct A_UIElemProp {
};

struct ShapeElem {
    struct A_UIElemProp elem;
    A_Move moves;
};

typedef struct A_ShapeElem {
    struct ShapeElem* data;
    size_t size;
    size_t offset;
} Shape;

/**
 * @brief
 *  Find a unique shape describing the searched element.
 * @param runtimeId
 *  The runtime id of the element we want to find a unique shape.
 * @param shape
 *  The returned shape that describes the element.
 */
error_t findUniqueShape(int32_t runtimeId, Shape* shape);

/**
 * @brief
 *  Function that finds and retrieves by runtime id.
 * @param name
 *  Name of the element being search.
 * @param targetElem
 *  Out parameter receiving the matching elements.
 * @return
 *  Error if something went wrong.
 */
error_t findElemByRunId(int32_t runtimeId, IUIAutomationElement** targetElem);

/**
 * @brief
 *  Function that finds and retrieves by name.
 * @param name
 *  Name of the element being search.
 * @param targetElem
 *  Out parameter receiving the matching elements.
 * @return
 *  Error if something went wrong.
 */
error_t findElementByName(
    std::wstring            name,
    IUIAutomationElement*   sRoot,
    enum TreeScope          scope,
    IUIAutomationElement**  targetElem
);

/**
 * @param appInfo
 *  Information about the application window that want to be searched.
 */
error_t findWindow(AppInfo appInfo, IUIAutomationElement** window);

/**
 * @brief
 *  Updates the queue with a new event when a window from the app described by parameter
 *  appinfo is openned.
 * @param appInfo
 *  Information about the application window which oppening we want to detect.
 * @param f
 *  Function to process desired info about the window.
 * @return
 *  Function returns E_FN_SUCCESS(0) or one of the following error codes:
 *      - E_UI_RTS_INIT in case the RTS for window handling wasn't initialilzed.
 *      - E_GET_ROOT_NODE if was a problem retrieving root element.
 */
error_t detectWindowOpening(
    AppInfo             appInfo,
    uint64_t            eventId,
    SVar<UIEventQueue>& sUIEventQueue,
    SVar<LogQueue>&     sLogQueue,
    EventHndResrcs&     resrcs
);

/**
 * @brief
 *  Updates the queue with a new event when a window from the app described by parameter
 *  appinfo is close.
 * @param appInfo
 *  Information about the application window which oppening we want to detect.
 * @param f
 *  Function to process desired info about the window.
 * @return
 *  Function returns E_FN_SUCCESS(0) or one of the following error codes:
 *      - E_UI_RTS_INIT in case the RTS for window handling wasn't initialilzed.
 *      - E_GET_ROOT_NODE if was a problem retrieving root element.
 */
error_t detectWindowClosing(AppInfo appInfo, UIEventQueue& eventQueue, EventHndResrcs& eventResrcs);

/**
 * @param name
 *  Name of the element to be called with a certain action.
 * @param action
 *  Out parameter receiving the matching elements.
 * @return
 *  Function returns E_FN_SUCCESS(0) or one of the following error codes:
 *      - E_UI_RTS_INIT in case the RTS for window handling wasn't initialilzed.
 *      - *** TODO: List need to be completed. ***
 */
error_t triggerElementByName(std::wstring name, IUIAutomationElement* sRoot, struct UIElemAction action);

/**
 * @param eventHnd
 *  Set a 'structure changed event handler' which detects a change in tree UI structure.
 * @param target
 *  Element in which the structural event detector is going to be attached.
 * @return
 *  Error if not able to set structure change event handler.
 *  Function returns E_FN_SUCCESS(0) or one of the following error codes:
 *      - E_UI_RTS_INIT in case the RTS for window handling wasn't initialilzed.
 *      - *** TODO: List need to be completed. ***
 */
error_t detectStructuralChange(IUIAutomationStructureChangedEventHandler* eventHnd, IUIAutomationElement* target);

/**
 * @brief
 *  Function that dectects when an element have been invalidated.
 * @param wRoot
 *  The window in which resides the element that we want to be tracked.
 * @param elem
 *  The automation element which invalidation wants to be tracked.
 * @return
 *  Error if something went wrong.
 */
error_t detectElementInvalidation(
    IUIAutomationElement*   wRoot,
    IUIAutomationElement*   targetElem,
    UIEventQueue&           evetQueue,
    EventHndResrcs&         syncData
);

#endif
