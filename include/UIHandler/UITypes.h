#ifndef UIH_UITYPES_H
#define UIH_UITYPES_H

#include <CVariant/Variant.h>
#include <Std/Vector.h>

#include <UIAutomation.h>

#include <queue>
#include <cstddef>
#include <optional>

/**
 * @brief
 *  Enum with all the possible actions to be executed over an element.
 */
enum UIElemActionCode {
    Action_Invoke,
    Action_ToogleExpandCollapse,
    Action_Expand,
    Action_Collapse,
    Action_Write,
    Action_Select,
    Action_ENODEF
};

/**
 * @brief
 *  Struct for holding an action over an element, and a payload needed to execute the action.
 * @var Action::code
 *  Action that is going to be performed over certain element.
 * @var Action::pl
 *  Payload with the necessary data for executing the action.
 */
struct UIElemAction {
    UIElemActionCode    code;
    Variant             pl;
};

/**
 * @brief Types of events that could be produced in the UI that we want to be able
 *        to observ.
 */
enum UIEventType {
    UIEventWinOpened,
    UIEventWinClosed,
    UIEventElemRemoved,
    UIEventElemInvalidated,
    UIEventChangedAppMode
};

/**
 * @brief
 *  The information a event transmit back to the service when it's triggered.
 * @var type
 *  The type of UI event that has been triggered.
 * @var appInfo
 *  The info about the application to which the triggered event is related.
 * @var elem
 *  The element that has triggered the event.
 */
struct UIEvent {
    uint64_t                eventId;
    uint64_t                appId;
    UIEventType             type;
    Variant                 eventPl;
    std::wstring            elemId;
    IUIAutomationElement*   elem;
    errno_t                 err;
};

using UIEventQueue = std::queue<UIEvent>;

/**
 * @struct AppInfo
 * @brief
 *  Information needed to find an application window.
 * @var AppInfo::appId
 *  Id for application identification.
 * @var AppInfo::name
 *  Name of the application.
 * @var AppInfo::path
 *  Application executable path in the filesystem.
 */
struct AppInfo {
    uint64_t    appId;
    std::string name;
    std::string path;
};

#endif
