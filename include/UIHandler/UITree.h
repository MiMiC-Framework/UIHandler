#ifndef UI_TREE_H
#define UI_TREE_H

#ifdef _WIN32
#include <windows.h>
#elif __linux
#include <X11/X.h>
#endif

#include <stdint.h>
#include <uchar.h>

#include <CArray/Array.h>
#include <CVariant/Variant.h>

extern "C" {

struct CmpProps {
    const char* props;
};

enum CmpType {
    CtrlCmp = 0,
    ContCmp = 1
};

struct CtrlCmp;
struct ContCmp;
struct RawCmp;

struct CtrlCmpTree;
struct ContCmpTree;
struct RawCompTree;

/**
 * @brief Type that represent the three types of trees that we can expose.
 * @details
 * 'CtrlTreeType': Holds the 'Control Elements' this elements are the
 * ones that the user use to control the application. In the Aria ontology we
 * are talking about the abstract role 'Widgets'.
 * 'ContTreeType': Holds the 'Contents Elements' those whose shows pure content,
 * without any kind of user interaction. In the Aria ontology we are
 * are talking about the abstract role 'Widgets'.
 * 'RawTreeType': Combines both previous trees into one.
 */
enum TreeType {
    CtrlTreeType = 0,
    ContTreeType = 1,
    RawTreeType = 2
};

struct UITree;
struct UIElemProperty;

//          For analyze now             //
// ==================================== //
// UIE_GroupControlTypeId,              //
// UIE_MenuControlTypeId,               //
// UIE_PaneControlTypeId,               //
// UIE_ToolBarControlTypeId,            //
// UIE_WindowControlTypeId,             //
// UIE_IsRequiredForFormPropertyId,     //
// UIE_ItemStatusPropertyId,            //

// UIE_E_ELEMENTNOTAVAILABLE,           //
// This is an Error Code                //

//              READ                    //
// ==================================== //
// UIE_CustomLandmarkTypeId,            //

enum UIElemRoles {
//  https://www.w3.org/TR/wai-aria-1.1/#dialog
//  Role
    UIE_Dialog,                         // UIE_AnnotationObjectsPropertyId,               // VT_Int32 | VT_ARRAY
};

/**
 * @enum Widget
 * @details
 *
 */
/**
 * @var WidgetType::UIE_Widget_Button
 * @brief Button element
 * Specifications mapping:
 *  - Aria: <a href="https://www.w3.org/TR/wai-aria-1.1/#button">button</a>
 *  - UIA: <a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671619(v=vs.85).aspx">Button Control Type</a>
 * .
 * <strong>Type:</strong> Role
 */
/**
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671620(v=vs.85).aspx">Calendar Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671621(v=vs.85).aspx">CheckBox Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671622(v=vs.85).aspx">ComboBox Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671623(v=vs.85).aspx">DataGrid Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671624(v=vs.85).aspx">DataItem Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671625(v=vs.85).aspx">Document Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671626(v=vs.85).aspx">Edit Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671628(v=vs.85).aspx">Group Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671629(v=vs.85).aspx">Header Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671630(v=vs.85).aspx">HeaderItem Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671631(v=vs.85).aspx">Hyperlink Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671632(v=vs.85).aspx">Image Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671634(v=vs.85).aspx">List Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671635(v=vs.85).aspx">ListItem Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671637(v=vs.85).aspx">Menu Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671636(v=vs.85).aspx">MenuBar Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671638(v=vs.85).aspx">MenuItem Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671639(v=vs.85).aspx">Pane Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671640(v=vs.85).aspx">ProgressBar Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671641(v=vs.85).aspx">RadioButton Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671642(v=vs.85).aspx">ScrollBar Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/hh920984(v=vs.85).aspx">SemanticZoom Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671643(v=vs.85).aspx">Separator Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671644(v=vs.85).aspx">Slider Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671645(v=vs.85).aspx">Spinner Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671646(v=vs.85).aspx">SplitButton Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671647(v=vs.85).aspx">StatusBar Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671648(v=vs.85).aspx">Tab Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671649(v=vs.85).aspx">TabItem Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671650(v=vs.85).aspx">Table Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671651(v=vs.85).aspx">Text Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671652(v=vs.85).aspx">Thumb Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671653(v=vs.85).aspx">TitleBar Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671654(v=vs.85).aspx">ToolBar Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671655(v=vs.85).aspx">ToolTip Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671656(v=vs.85).aspx">Tree Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671657(v=vs.85).aspx">TreeItem Control Type</a>
<a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee671658(v=vs.85).aspx">Window Control Type</a>
*/
enum WidgetType {
    UIE_Widget_Button,
    UIE_Widget_Checkbox,
    UIE_Widget_GridCell,
    UIE_Widget_Link,
    UIE_Widget_MenuItem,
    UIE_Widget_MenuItemCheckbox,
    UIE_Widget_MenuItemRadio,
    UIE_Widget_Option,
    UIE_Widget_ProgressBar,
    UIE_Widget_Radio,
    UIE_Widget_Scrollbar,
    UIE_Widget_Searchbox,
    UIE_Widget_Slider,
    UIE_Widget_Spinbutton,
    UIE_Widget_Switch,
    UIE_Widget_Tab,
    UIE_Widget_Textbox,
    UIE_Widget_Timer,
    UIE_Widget_Tooltip,
    UIE_Widget_TreeItem
};

/**
 * @enum UIElemProp
 * @brief
 * Enum that holds all the properties that can be present in the UI Elements
 * based on the Aria standard based attributes.
 */
/**
 * @var UIElemProp::UIE_Prop_KeyShortCuts
 * @brief <strong>Aria Attribute Property</strong>
 * @details
 * Specifications mapping:
 *  - Aria: <a href="https://www.w3.org/TR/wai-aria-1.1/#aria-keyshortcuts">aria-keyshortcuts</a>
 *  - UIA: UIA_AcceleratorKeyPropertyId
 * .
 * <strong>Type:</strong> VT_BSTR
 */
/**
 * @var UIElemProp::UIE_Prop_AccessKey
 * @brief <strong>HTML Attribute</strong>
 * @details
 * Specifications mapping:
 *  - HTML: <a href="https://www.w3.org/TR/html/editing.html#element-attrdef-global-accesskey">accesskey</a>
 *  - UIA: UIA_AccessKeyPropertyId
 * .
 * <strong>Type:</strong> VT_BSTR
 */
/**
 * @var UIElemProp::UIE_Prop_Role
 * @brief <strong>HTML Attribute</strong>
 * @details
 * Specifications mapping:
 *  - HTML: <a href="https://www.w3.org/TR/role-attribute">role</a>
 *  - UIA: UIA_AriaRolePropertyId
 * .
 * <strong>Type:</strong> VT_BSTR
 */
/**
 * @var UIElemProp::UIE_Prop_Id
 * @brief <strong>Aria Extension</strong>
 * @details
 * Specifications mapping:
 *  - HTML: <a href="https://developer.mozilla.org/en/docs/Web/HTML/Global_attributes/id">id</a>
 *  - UIA: UIA_AutomationIdPropertyId
 * .
 * <strong>Type:</strong> VT_BSTR
 */
/**
 * @var UIElemProp::UIE_Prop_BoundingRectangle
 * @brief <strong>Aria Extension</strong>
 * @details
 * This is an extension to the Aria spec.<br>
 * <br>
 * Specifications mapping:
 *  - HTML: ??
 *  - UIA: UIA_BoundingRectanglePropertyId
 * .
 * <strong>Type:</strong> VT_R8 | VT_Array
 */
/**
 * @var UIElemProp::UIE_Prop_Point
 * @brief <strong>Aria Extension</strong>
 * @details
 * This is an extension to the Aria spec.<br>
 * <br>
 * Specifications mapping:
 *  - HTML: ??
 *  - UIA: UIA_CenterPointPropertyId
 * .
 * <strong>Type:</strong> VT_R8 | VT_ARRAY || Default Value: VT_EMPTY
 */
/**
 * @var UIElemProp::UIE_Prop_Point
 * @brief <strong>Aria Extension</strong>
 * @details
 * This is an extension to the Aria spec.<br>
 * <br>
 * Specifications mapping:
 *  - HTML: ??
 *  - UIA: UIA_CenterPointPropertyId
 * .
 * <strong>Type:</strong> VT_R8 | VT_ARRAY || Default Value: VT_EMPTY
 */
/**
 * @var UIElemProp::UIE_Prop_TypeImplRole
 * @brief <strong>Aria Extension</strong>
 * @details
 * Specifications mapping:
 *  - HTML: ??
 *  - UIA: UIA_ClassNamePropertyId
 * .
 * <strong>Type:</strong> VT_BSTR
 */
/**
 * @var UIElemProp::UIE_Prop_ClickablePoints
 * @brief <strong>Aria Extension</strong>
 * @details
 * Specifications mapping:
 *  - HTML: ??
 *  - UIA: UIE_ClickablePointPropertyId
 * .
 * <strong>Type:</strong> Providers: VT_R8 | VT_ARRAY || Default Value: VT_EMPTY
 */
/**
 * @var UIElemProp::UIE_Prop_Controls
 * @brief <strong>Aria Attribute Property</strong>
 * @details
 * Specifications mapping:
 *  - HTML: ??
 *  - UIA: UIA_ControllerForPropertyId
 * .
 * <strong>Type:</strong> Providers: VT_UNKNOWN | VT_ARRAY || Client: VT_UNKNOWN
 */
/**
 * @var UIElemProp::UIE_Prop_ControlType
 * @brief <strong>Aria Extension</strong>
 * @details
 * Specifications mapping:
 *  Roles:<br>
 *  This property doesn't have direct of indirect approach, describes elements in<br>
 *  terms of the types described by UI Automation. In Aria, you directly expose certain<br>
 *  elements, as certain controls, you don't expose them as a common interface with a<br>
 *  'identification property'.<br>
 *  - HTML: ??
 *  - UIA: UIA_ControlTypePropertyId
 * .
 * <strong>Type:</strong> VT_Int32 | Default Value: UIA_CustomControlTypeId
 */
/**
 * @var UIElemProp::UIE_Prop_Lang
 * @brief <strong>HTML Attribute</strong>
 * @details
 * Specifications mapping:
 *  - HTML: https://www.w3.org/TR/html5/dom.html#the-lang-and-xml:lang-attributes
 *  - UIA: UIA_CulturePropertyId
 * .
 * <strong>Type:</strong> Providers: VT_Int32
 */
/**
 * @var UIElemProp::UIE_Prop_Details
 * @brief <strong>Aria Attribute Property</strong>
 * @details
 * Specifications mapping:
 *  - HTML: https://www.w3.org/TR/wai-aria-1.1/#aria-details
 *  - UIA: UIA_DescribedByPropertyId
 * .
 * <strong>Type:</strong> Providers:  VT_UNKNOWN | VT_ARRAY
 */
/**
 * @var UIElemProp::UIE_Prop_BackgroundColor
 * @brief <strong>CSS Property</strong>
 * @details
 * Specifications mapping:
 *  - HTML: https://www.w3schools.com/cssref/pr_background-color.asp
 *  - UIA: UIA_FillColorPropertyId
 * .
 * <strong>Type:</strong> Providers: VT_Int32
 */
/**
 * @var UIElemProp::UIE_Prop_FillType
 * @brief <strong>Extension - Map from UIA</strong>
 * @details
 * Specifications mapping:
 *  - HTML: ??
 *  - UIA: <a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee684017(v=vs.85).aspx">UIA_FillTypePropertyId</a>
 * .
 * <strong>Type:</strong> Providers: VT_Int32
 */
/**
 * @var UIElemProp::UIE_Prop_FlowFrom
 * @brief <strong>Extension - Map from UIA</strong>
 * @details
 * Specifications mapping:
 *  - HTML: ??
 *  - UIA: <a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ee684017(v=vs.85).aspx">UIA_FillTypePropertyId</a>
 * .
 * <strong>Type:</strong> Providers: Providers: VT_UNKNOWN | VT_ARRAY || Client: VT_UNKNOWN
 */
/**
 * @var UIElemProp::UIE_Prop_FlowTo
 * @brief <strong>Aria Attribute Property</strong>
 * @details
 * Specifications mapping:
 *  - Aria: <a href="https://www.w3.org/TR/wai-aria-1.1/#aria-flowto">aria-flowto</a>
 *  - UIA: UIA_FlowsToPropertyId
 * .
 * <strong>Type:</strong> Providers: Providers: VT_UNKNOWN | VT_ARRAY || Client: VT_UNKNOWN
 */
/**
 * @var UIElemProp::UIE_Prop_DescribedBy
 * @brief <strong>Aria Attribute Property</strong>
 * @details
 * Specifications mapping:
 *  - Aria: <a href="https://www.w3.org/TR/wai-aria-1.1/#aria-roledescription">aria-roledescription</a>
 *  - UIA: UIA_FullDescriptionPropertyId
 * .
 * <strong>Type:</strong> VT_BSTR
 */
/**
 * @var UIElemProp::UIE_Prop_ActiveElement
 * @brief <strong>DOM_Property</strong>
 * @details
 * Notes: Problem is that this element doesn't resemble the actual functionality<br>
 * in most accessibility API, in those, this is a global property, not a global,<br>
 * shared property with the current active element.<br>
 * <br>
 * Specifications mapping:
 *  - Aria: <a href="https://developer.mozilla.org/en-US/docs/Web/API/Document/activeElement">Document.activeElement</a>
 *  - UIA: UIA_HasKeyboardFocus
 * .
 * <strong>Type:</strong> VT_bool
 */
/**
 * @var UIElemProp::UIE_Prop_LabelledBy
 * @brief <strong>Aria Attribute Property</strong>
 * @details
 * Specifications mapping:
 *  - Aria: <a href="https://w3c.github.io/aria/aria/aria.html#aria-labelledby">aria-labelledby</a>
 *  - UIA: UIA_HelpTextPropertyId
 * .
 * <strong>Type:</strong> VT_BSTR
 */
/**
 * @var UIElemProp::UIE_Prop_ContentElement
 * @brief <strong>Extension - Map from UIA</strong>
 * @details
 * Specifications mapping:
 *  - Aria: ??
 *  - UIA: UIA_IsContentElementPropertyId
 * .
 * <strong>Type:</strong> VT_bool
 */
/**
 * @var UIElemProp::UIE_Prop_Invalid
 * @brief <strong>Aria Attribute Property State</strong>
 * @details
 * Specifications mapping:
 *  - Aria: <a href="https://w3c.github.io/aria/aria/aria.html#aria-invalid">aria-invalid</a>
 *  - UIA: UIA_IsDataValidForFormPropertyId
 * .
 * <strong>Type:</strong> VT_bool
 */
/**
 * @var UIElemProp::UIE_Prop_Disabled
 * @brief <strong>Aria Attribute Property State</strong>
 * @details
 * Specifications mapping:
 *  - Aria: <a href="https://w3c.github.io/aria/aria/aria.html#aria-disabled">aria-disabled</a>
 *  - UIA: UIA_IsEnabledPropertyId
 * .
 * <strong>Type:</strong> VT_bool
 */
/**
 * @var UIElemProp::UIE_Prop_TabIndex
 * @brief <strong>HTML Attribute</strong>
 * @details
 * Following the Aria recomendation this element should be "Tabindex = 0", in the<br>
 * case of being keyboard focusable or "Tabindex = -1" in the case of being<br>
 * non-keyboard focusable.<br>
 * <br>
 * Specifications mapping:
 *  - HTML: <a href="">aria-disabled</a>
 *  - UIA: UIA_IsKeyboardFocusablePropertyId
 * .
 * <strong>Type:</strong> VT_bool
 */
/**
 * @var UIElemProp::UIE_Prop_Hidden
 * @brief <strong>Aria Attribute Property State</strong>
 * @details
 * Specifications mapping:
 *  - Aria: <a href="https://www.w3.org/TR/wai-aria/states_and_properties#aria-hidden">aria-hidden</a>
 *  - UIA: UIA_IsOffscreenPropertyId
 * .
 * <strong>Type:</strong> VT_bool
 */
/**
 * @var UIElemProp::UIE_Prop_Password
 * @brief <strong>HTML Input Type</strong>
 * @details
 * Specifications mapping:
 *  - HTML: <a href="https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/password">input="password"</a>
 *  - UIA: UIA_IsPasswordPropertyId
 * .
 * <strong>Type:</strong> VT_BSTR
 */
/**
 * @var UIElemProp::UIE_Prop_Required
 * @brief <strong>Aria Attribute Property</strong>
 * @details
 * Specifications mapping:
 *  - Aria: <a href="https://w3c.github.io/aria/aria/aria.html#aria-required">aria-required</a>
 *  - UIA: UIA_IsRequiredForFormPropertyId
 * .
 * <strong>Type:</strong> VT_bool
 */
/**
 * @var UIElemProp::UIE_Prop_Status
 * @brief <strong>Aria Role</strong>
 * @details
 * Specifications mapping:
 *  - Aria: <a href="https://www.w3.org/TR/wai-aria-1.1/#status">aria</a>
 *  - UIA: UIA_ItemStatusPropertyId
 * .
 * <strong>Type:</strong> VT_BSTR
 */
/**
 * @var UIElemProp::UIE_Prop_LabeledBy
 * @brief <strong>HTML Attribute</strong>
 * @details
 * Specifications mapping:
 *  - Aria: <a href="https://w3c.github.io/aria/aria/aria.html#aria-labelledby">aria-labelledby</a>
 *  - UIA: UIA_LabeledByPropertyId
 * .
 * <strong>Type:</strong> VT_UNKNOWN
 */
/**
 * @var UIElemProp::UIE_Prop_Level
 * @brief <strong>Aria Attribute Property</strong>
 * @details
 * Specifications mapping:
 *  - Aria: <a href="https://w3c.github.io/aria/aria/aria.html#aria-level">aria-level</a>
 *  - UIA: UIA_LevelPropertyId
 * .
 * <strong>Type:</strong> VT_Int32
 */
/**
 * @var UIElemProp::UIE_Prop_Live
 * @brief <strong>Aria Attribute Property</strong>
 * @details
 * Specifications mapping:
 *  - Aria: <a href="https://w3c.github.io/aria/aria/aria.html#aria-live">aria-live</a>
 *  - UIA: UIA_LiveSettingPropertyId
 * .
 * <strong>Type:</strong> VT_Int32
 */
/**
 * @var UIElemProp::UIE_Prop_Name
 * @brief <strong>HTML Attribute</strong>
 * @details
 * Specifications mapping:
 *  - HTML: <a href="https://www.w3.org/TR/html51/syntax.html#name">name</a>
 *  - UIA: UIA_NamePropertyId
 * .
 * <strong>Type:</strong> VT_BSTR
 */
/**
 * @var UIElemProp::UIE_Prop_WindowHandler
 * @brief <strong>Extension - Map from UIA</strong>
 * @details
 * Specifications mapping:
 *  - Aria: ??
 *  - UIA: UIA_NativeWindowHandlePropertyId
 * .
 * <strong>Type:</strong> VT_Int32
 */
/**
 * @var UIElemProp::UIE_Prop_PosInSet
 * @brief <strong>Aria Attribute Property</strong>
 * @details
 * Specifications mapping:
 *  - Aria: ??
 *  - UIA: UIA_PositionInSetPropertyId
 * .
 * <strong>Type:</strong> VT_Int32
 */
/**
 * @var UIElemProp::UIE_Prop_ProcessId
 * @brief <strong>Extension - Map from UIA</strong>
 * @details
 * Specifications mapping:
 *  - Aria: ??
 *  - UIA: UIA_ProcessIdPropertyId
 * .
 * <strong>Type:</strong> VT_Int32
 */
/**
 * @var UIElemProp::UIE_Prop_ProviderDescription
 * @brief <strong>Extension - Map from UIA</strong>
 * @details
 * Specifications mapping:
 *  - Aria: ??
 *  - UIA: UIA_ProviderDescriptionPropertyId
 * .
 * <strong>Type:</strong> VT_R8 | VT_ARRAY || Default Val: VT_EMPTY
 */
/**
 * @var UIElemProp::UIE_Prop_Size
 * @brief <strong>Extension - Map from UIA</strong>
 * @details
 * Specifications mapping:
 *  - Aria: ??
 *  - UIA: UIA_SizePropertyId
 * .
 * <strong>Type:</strong> VT_BSTR
 */
/**
 * @var UIElemProp::UIE_Prop_SetSize
 * @brief <strong>Aria Attribute Property</strong>
 * @details
 * Specifications mapping:
 *  - Aria: ??
 *  - UIA: UIA_SizeOfSetPropertyId
 * .
 * <strong>Type:</strong> VT_Int32
 */
enum UIElemProp {
    UIE_Prop_KeyShortCuts,
    UIE_Prop_AccessKey,
//  - No Map -
//  UIE_?                                       // UIA_AnnotationTypesPropertyId,              // VT_Int32 | VT_ARRAY
//  - No Map -
//  UIE_?                                       // UIA_AriaPropertiesPropertyId,               // VT_BSTR
    UIE_Prop_Role,
    UIE_Prop_Id,
    UIE_Prop_BoundingRectangle,
    UIE_Prop_Point,
    UIE_Prop_TypeImplRole,
    UIE_Prop_ClickablePoints,
    UIE_Prop_Controls,
    UIE_Prop_ControlType,
    UIE_Prop_Lang,
    UIE_Prop_Details,
    UIE_Prop_BackgroundColor,
    UIE_Prop_FillType,
    UIE_Prop_FlowFrom,
    UIE_Prop_FlowTo,
//  - No Map - or need, backend handled.
//  UIE_?,                                      // UIA_FrameworkIdPropertyId,                  // VT_BSTR
    UIE_Prop_DescribedBy,
    UIE_Prop_ActiveElement,
    UIE_Prop_LabelledBy,
    UIE_Prop_ContentElement,
    UIE_Prop_Invalid,
    UIE_Prop_Disabled,
    UIE_Prop_TabIndex,
    UIE_Prop_Hidden,
    UIE_Prop_Password,
    UIE_Prop_Required,
    UIE_Prop_Status,
//  - No Map -
//  UIE_?,                                      // UIA_ItemTypePropertyId,                     // VT_BSTR
//  Aria Attribute Property
    UIE_Prop_LabeledBy,
//  - No Map -
//  UIE_?                                       // UIA_LandmarkTypePropertyId,                 // VT_Int32
    UIE_Prop_Level,
    UIE_Prop_Live,
//  Aria Attribute Property
//  UIE_?,                                      // UIA_LocalizedControlTypePropertyId,         // VT_BSTR
//  Aria Attribute Property
//  UIE_?,                                      // UIA_LocalizedLandmarkTypePropertyId,        // VT_BSTR
    UIE_Prop_Name,
//  Extension - Map from UIA - UIA_NativeWindowHandlePropertyId ** Needed? **
    UIE_Prop_WindowHandler,

//  NOT NEEDED
//  ============================================================================
//  UIA_OptimizeForVisualContentPropertyId,     // VT_bool
//  UIA_OrientationPropertyId,                  // VT_Int32
//  UIA_OutlineColorPropertyId,                 // VT_Int32 | VT_ARRAY
//  UIA_OutlineThicknessPropertyId,             // VT_R8 | VT_ARRAY || Default Val: VT_EMPTY
//  ============================================================================

    UIE_Prop_PosInSet,
    UIE_Prop_ProcessId,
    UIE_Prop_ProviderDescription,
//  - No Map -
//  UIE_RotationPropertyId,                     // VT_R8
//  - No Map -
//  UIE_RuntimeIdPropertyId,                    // VT_Int32 | VT_ARRAY || Default Val: VT_EMPTY
    UIE_Prop_Size,
    UIE_Prop_SetSize,
//  - No Map -
//  UIE_VisualEffectsPropertyId,                // VT_Int32
};

/**
 * @struct UIElement
 * @brief
 * @details
 */
/**
 * @var UIElement::Controls
 * @brief UIE_Prop_Controls
 */
/**
 * @var UIElement::Controls
 * @brief UIE_Prop_Controls
 */
/**
 * @var UIElement::Current
 */
struct UIElement {
//  - No Map -
//  Atomic,     // VT_bool
//  - No Map -
//  Busy,       // VT_bool
//  - No Map -
    struct GArray* Controls;
};

// enum UIAControlsProps {
//     UIA_AnnotationAnnotationTypeIdPropertyId,             // VT_Int32
//     UIA_AnnotationAnnotationTypeNamePropertyId,           // VT_BSTR
//     UIA_AnnotationAuthorPropertyId,                       // VT_BSTR
//     UIA_AnnotationDateTimePropertyId,                     // VT_BSTR
//     UIA_AnnotationTargetPropertyId,                       // VT_UNKNOWN
//     UIA_DockDockPositionPropertyId,                       // VT_Int32
//     UIA_DragDropEffectPropertyId,                         // VT_BSTR
//     UIA_DragDropEffectsPropertyId,                        // VT_BSTR | VT_ARRAY
//     UIA_DragIsGrabbedPropertyId,                          // VT_bool
//     UIA_DragGrabbedItemsPropertyId,                       // VT_UNKNOWN | VT_ARRAY
//     UIA_DropTargetDropTargetEffectPropertyId,             // VT_BSTR
//     UIA_DropTargetDropTargetEffectsPropertyId,            // VT_BSTR | VT_ARRAY
//     UIA_ExpandCollapseExpandCollapseStatePropertyId,      // VT_Int32
//     UIA_GridColumnCountPropertyId,                        // VT_Int32
//     UIA_GridItemColumnPropertyId,                         // VT_Int32
//     UIA_GridItemColumnSpanPropertyId,                     // VT_Int32
//     UIA_GridItemContainingGridPropertyId,                 // VT_UNKNOWN
//     UIA_GridItemRowPropertyId,                            // VT_Int32
//     UIA_GridItemRowSpanPropertyId,                        // VT_Int32
//     UIA_GridRowCountPropertyId,                           // VT_Int32
//     UIA_LegacyIAccessibleChildIdPropertyId,               // VT_Int32
//     UIA_LegacyIAccessibleDefaultActionPropertyId,         // VT_BSTR
//     UIA_LegacyIAccessibleDescriptionPropertyId,           // VT_BSTR
//     UIA_LegacyIAccessibleHelpPropertyId,                  // VT_BSTR
//     UIA_LegacyIAccessibleKeyboardShortcutPropertyId,      // VT_BSTR
//     UIA_LegacyIAccessibleNamePropertyId,                  // VT_BSTR
//     UIA_LegacyIAccessibleRolePropertyId,                  // VT_Int32
//     UIA_LegacyIAccessibleSelectionPropertyId,             // VT_UNKNOWN | VT_ARRAY
//     UIA_LegacyIAccessibleStatePropertyId,                 // VT_Int32
//     UIA_LegacyIAccessibleValuePropertyId,                 // VT_BSTR
//     UIA_MultipleViewCurrentViewPropertyId,                // VT_Int32
//     UIA_MultipleViewSupportedViewsPropertyId,             // VT_Int32 | VT_ARRAY
//     UIA_RangeValueIsReadOnlyPropertyId,                   // VT_bool
//     UIA_RangeValueLargeChangePropertyId,                  // VT_R8
//     UIA_RangeValueMaximumPropertyId,                      // VT_R8
//     UIA_RangeValueMinimumPropertyId,                      // VT_R8
//     UIA_RangeValueSmallChangePropertyId,                  // VT_R8
//     UIA_RangeValueValuePropertyId,                        // VT_R8
//     UIA_ScrollHorizontallyScrollablePropertyId,           // VT_bool
//     UIA_ScrollHorizontalScrollPercentPropertyId,          // VT_R8
//     UIA_ScrollHorizontalViewSizePropertyId,               // VT_R8
//     UIA_ScrollVerticallyScrollablePropertyId,             // VT_bool
//     UIA_ScrollVerticalScrollPercentPropertyId,            // VT_R8
//     UIA_ScrollVerticalViewSizePropertyId,                 // VT_R8
//     UIA_SelectionCanSelectMultiplePropertyId,             // VT_bool
//     UIA_SelectionIsSelectionRequiredPropertyId,           // VT_bool
//     UIA_SelectionSelectionPropertyId,                     // VT_UNKNOWN | VT_ARRAY
//     UIA_SelectionItemIsSelectedPropertyId,                // VT_bool
//     UIA_SelectionItemSelectionContainerPropertyId,        // VT_UNKNOWN
//     UIA_SpreadsheetItemFormulaPropertyId,                 // VT_BSTR
//     UIA_SpreadsheetItemAnnotationObjectsPropertyId,       // VT_UNKNOWN | VT_ARRAY
//     UIA_SpreadsheetItemAnnotationTypesPropertyId,         // VT_Int32 | VT_ARRAY
//     UIA_StylesExtendedPropertiesPropertyId,               // VT_BSTR
//     UIA_StylesFillColorPropertyId,                        // VT_Int32
//     UIA_StylesFillPatternColorPropertyId,                 // VT_Int32
//     UIA_StylesFillPatternStylePropertyId,                 // VT_BSTR
//     UIA_StylesShapePropertyId,                            // VT_BSTR
//     UIA_StylesStyleIdPropertyId,                          // VT_Int32
//     UIA_StylesStyleNamePropertyId,                        // VT_BSTR
//     UIA_TableColumnHeadersPropertyId,                     // VT_UNKNOWN | VT_ARRAY
//     UIA_TableItemColumnHeaderItemsPropertyId,             // VT_UNKNOWN | VT_ARRAY
//     UIA_TableRowHeadersPropertyId,                        // VT_UNKNOWN | VT_ARRAY
//     UIA_TableRowOrColumnMajorPropertyId,                  // VT_Int32
//     UIA_TableItemRowHeaderItemsPropertyId,                // VT_UNKNOWN | VT_ARRAY
//     UIA_ToggleToggleStatePropertyId,                      // VT_Int32
//     UIA_TransformCanMovePropertyId,                       // VT_bool
//     UIA_TransformCanResizePropertyId,                     // VT_bool
//     UIA_TransformCanRotatePropertyId,                     // VT_bool
//     UIA_Transform2CanZoomPropertyId,                      // VT_bool
//     UIA_Transform2ZoomLevelPropertyId,                    // VT_R8
//     UIA_Transform2ZoomMaximumPropertyId,                  // VT_R8
//     UIA_Transform2ZoomMinimumPropertyId,                  // VT_R8
//     UIA_ValueIsReadOnlyPropertyId,                        // VT_bool
//     UIA_ValueValuePropertyId,                             // VT_BSTR
//     UIA_WindowCanMaximizePropertyId,                      // VT_bool
//     UIA_WindowCanMinimizePropertyId,                      // VT_bool
//     UIA_WindowIsModalPropertyId,                          // VT_bool
//     UIA_WindowIsTopmostPropertyId,                        // VT_bool
//     UIA_WindowWindowInteractionStatePropertyId,           // VT_Int32
//     UIA_WindowWindowVisualStatePropertyId                 // VT_Int32
// };

}

#endif

