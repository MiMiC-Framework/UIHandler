#ifndef UIH_ERROR_H
#define UIH_ERROR_H

#if (defined _WIN32)
#include <windows.h>
#elif (defined __linux)
#include <stdint.h>
#endif

#include <string>
#include <uerrno/uerrno.h>

extern "C" {

/**
 * @enum EIWHandler
 * @brief Enum with all the possible errors that functions related with UI interaction can retrieve.
 */
/**
 * @var uierr::E_UI_RTS_INIT
 * @brief Invalid RTS initialization.
 * @details
 * The Window Handler implementer needs to provide a specific message through the
 * function whStrError.
 */
/**
 * @var uierr::E_UI_RTS_CLOSE
 * @brief Invalid RTS shutdown.
 * @details
 * The Window Handler implementer needs to provide a specific message through the
 * function whStrError.
 */
/**
 * @var uierr::E_UI_RTS_INVAL_STATE
 * @brief RTS is in a invalid state.
 * @details
 * The Window Handler implementer needs to provide a specific message through the
 * function whStrError.
 */
enum uierr {
    E_UI_GET_ROOT_NODE      = -50,
    E_UI_COND_CREATION      = -51,
    E_UI_GET_ELEM           = -52,
    E_UI_ELEM_NOT_FOUND     = -53,
    E_UI_ELEM_NOT_UNIQUE_ID = -54,
    E_UI_NOT_UNIQUE_SHAPE   = -55,
// =============================================================================
    E_UI_RTS_INIT           = -101,
    E_UI_RTS_CLOSE          = -102,
    E_UI_RTS_INVAL_STATE    = -103,
// =============================================================================
};

typedef uierr uierr_t;

}

std::string toString(uierr_t err);

#endif
