# README

### 1. What is this?

Library for exposing UI Automation APIs in a uniform and ARIA compliant way.

### 2. Future goals

Minimal implementation for simple UI Handling.

TODO:
    - Formal specification for the API, with requirement list.
    - Export everything as a C interface.

### 4. Usage (dev)

Setup and use as a Conan dependency:

conan remote add MiMiC https://bintray.com/javjarfer/MiMiC
conan install "UIHandler/0.1@javjarfer/testing"

#### Building

In project folder:

For debugging purposes:

1. mkdir build && cd build
2. conan install .. -s build_type=Debug  --build=missing -s compiler="Visual Studio" -s compiler.runtime="MDd"
3. cmake -G "Visual Studio 15 Win64" -DCMAKE_BUILD_TESTS=ON ..
4. cmake --build . --target ALL_BUILD --config Debug

For building a release version:

1. mkdir build && cd build
2. conan install .. -s build_type=Release  --build=missing
3. cmake -G "Visual Studio 15 Win64" -DCMAKE_BUILD_TESTS=ON ..
4. cmake --build . --target ALL_BUILD --config Release
